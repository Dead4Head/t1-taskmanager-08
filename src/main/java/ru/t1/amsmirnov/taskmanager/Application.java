package ru.t1.amsmirnov.taskmanager;

import ru.t1.amsmirnov.taskmanager.constant.CommandConst;
import ru.t1.amsmirnov.taskmanager.constant.ArgumentConst;
import ru.t1.amsmirnov.taskmanager.model.Command;
import ru.t1.amsmirnov.taskmanager.repository.CommandRepository;
import ru.t1.amsmirnov.taskmanager.util.FormatUtil;

import java.util.Scanner;


public final class Application {

    public static void main(String[] args) {
        processArguments(args);
        System.out.println("** WELCOME TASK MANAGER **");
        final Scanner scanner = new Scanner(System.in);
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND: ");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    public static void processArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        final String argument = args[0];
        processArgument(argument);
        System.exit(0);
    }

    public static void processArgument(final String argument) {
        switch (argument) {
            case ArgumentConst.HELP:
                showHelp();
                break;
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.INFO:
                showInfo();
                break;
            case ArgumentConst.ARGUMENTS:
                showArguments();
                break;
            case ArgumentConst.COMMANDS:
                showCommands();
                break;
            default:
                showArgumentError();
        }
    }

    public static void processCommand(final String command) {
        switch (command) {
            case CommandConst.HELP:
                showHelp();
                break;
            case CommandConst.ABOUT:
                showAbout();
                break;
            case CommandConst.VERSION:
                showVersion();
                break;
            case CommandConst.INFO:
                showInfo();
                break;
            case CommandConst.ARGUMENTS:
                showArguments();
                break;
            case CommandConst.COMMANDS:
                showCommands();
                break;
            case CommandConst.EXIT:
                exit();
                break;
            default:
                showCommandError();
        }
    }

    public static void showInfo() {
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long usageMemory = totalMemory - freeMemory;

        final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;

        final String freeMemoryFormat = FormatUtil.formatBytes(freeMemory);
        final String maxMemoryFormat = FormatUtil.formatBytes(maxMemory);
        final String maxMemoryValue = maxMemoryCheck ? "no limit" : maxMemoryFormat;
        final String totalMemoryFormat = FormatUtil.formatBytes(totalMemory);
        final String usageMemoryFormat = FormatUtil.formatBytes(usageMemory);


        System.out.println("[INFO]");
        System.out.printf("Available processors (cores): %s \n", availableProcessors);
        System.out.printf("Free memory: %s \n",freeMemoryFormat );
        System.out.printf("Maximum memory: %s \n", maxMemoryValue);
        System.out.printf("Total memory: %s \n", totalMemoryFormat);
        System.out.printf("Usage memory: %s \n", usageMemoryFormat);
    }

    public static void exit() {
        System.exit(0);
    }

    public static void showArgumentError() {
        System.err.println("[ERROR]");
        System.err.println("This argument is not supported!");
        System.exit(1);
    }

    public static void showCommandError() {
        System.err.println("[ERROR]");
        System.err.println("This command is not supported!");
        System.exit(1);
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("name: Smirnov Anton");
        System.out.println("email: amsmirnov@t1-consulting.ru");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.5.0");
    }

    public static void showCommands(){
        System.out.println("[COMMANDS]");
        final Command[] commands = CommandRepository.getTerminalCommands();
        for (final Command command: commands){
            if (command == null) continue;
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    public static void showArguments(){
        System.out.println("[ARGUMENTS]");
        final Command[] commands = CommandRepository.getTerminalCommands();
        for (final Command command: commands){
            if (command == null) continue;
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        Command[] commands = CommandRepository.getTerminalCommands();
        for (Command command: commands) System.out.println(command);
    }

}
