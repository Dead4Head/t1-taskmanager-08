package ru.t1.amsmirnov.taskmanager.repository;

import ru.t1.amsmirnov.taskmanager.constant.ArgumentConst;
import ru.t1.amsmirnov.taskmanager.constant.CommandConst;
import ru.t1.amsmirnov.taskmanager.model.Command;

public class CommandRepository {

    private static final Command HELP = new Command(CommandConst.HELP, ArgumentConst.HELP, "Show command list.");
    private static final Command ABOUT = new Command(CommandConst.ABOUT, ArgumentConst.ABOUT, "Show developer info.");
    private static final Command VERSION = new Command(CommandConst.VERSION, ArgumentConst.VERSION, "Show version info.");
    private static final Command INFO = new Command(CommandConst.INFO, ArgumentConst.INFO, "Show system information.");
    private static final Command ARGUMENTS = new Command(CommandConst.ARGUMENTS, ArgumentConst.ARGUMENTS, "Show available arguments.");
    private static final Command COMMANDS = new Command(CommandConst.COMMANDS, ArgumentConst.COMMANDS, "Show available commands.");
    private static final Command EXIT = new Command(CommandConst.EXIT, null, "Close Task Manager.");

    private static final Command[] TERMINAL_COMMANDS = new Command[] {HELP, ABOUT, VERSION, INFO, ARGUMENTS, COMMANDS, EXIT};

    public static Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
